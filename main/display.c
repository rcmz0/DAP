#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"

#include "display.h"
#include "pins.h"

#define DISPLAY_SPI SPI2_HOST

static spi_device_handle_t spi;
static bool waiting;

static void display_wait() {
    if (waiting) {
        spi_transaction_t* previous_transaction;
        spi_device_get_trans_result(spi, &previous_transaction, portMAX_DELAY);
        waiting = false;
    }
}

static void display_command(uint8_t command, uint8_t* args, size_t num_args) {
    display_wait();

	gpio_set_level(DISPLAY_DC_PIN, 0);
    spi_transaction_t command_transaction = {
        .tx_buffer = &command,
        .length = 8,
    };
    ESP_ERROR_CHECK(spi_device_polling_transmit(spi, &command_transaction));

	gpio_set_level(DISPLAY_DC_PIN, 1);
    if (num_args > 0) {
        spi_transaction_t args_transaction = {
            .tx_buffer = args,
            .length = num_args * 8,
        };
        ESP_ERROR_CHECK(spi_device_polling_transmit(spi, &args_transaction));
    }
}

static void display_command0(uint8_t command) {
    display_command(command, NULL, 0);
}

static void display_command1(uint8_t command, uint8_t arg1) {
    uint8_t data[] = { arg1 };
    display_command(command, data, 1);
}

static void display_command2(uint8_t command, uint8_t arg1, uint8_t arg2) {
    uint8_t data[] = { arg1, arg2 };
    display_command(command, data, 2);
}

static void display_command3(uint8_t command, uint8_t arg1, uint8_t arg2, uint8_t arg3) {
    uint8_t data[] = { arg1, arg2, arg3 };
    display_command(command, data, 3);
}

void display_init() {
    spi_bus_config_t spi_bus_config = {
        .sclk_io_num = DISPLAY_SCK_PIN,
        .mosi_io_num = DISPLAY_MOSI_PIN,
        .miso_io_num = -1,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = display_w * display_h * 2,
    };
    ESP_ERROR_CHECK(spi_bus_initialize(DISPLAY_SPI, &spi_bus_config, SPI_DMA_CH_AUTO));

    spi_device_interface_config_t spi_device_config = {
        .clock_speed_hz = 13000000,
        .mode = 0,
        .spics_io_num = -1,
        .queue_size = 1,
    };
    ESP_ERROR_CHECK(spi_bus_add_device(DISPLAY_SPI, &spi_device_config, &spi));

    gpio_set_direction(DISPLAY_DC_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(DISPLAY_RST_PIN, GPIO_MODE_OUTPUT);

    gpio_set_level(DISPLAY_RST_PIN, 0);
    vTaskDelay(100/portTICK_PERIOD_MS);
    gpio_set_level(DISPLAY_RST_PIN, 1);
    vTaskDelay(100/portTICK_PERIOD_MS);

	display_command1(0xFD, 0x12); // unlock commands
	display_command1(0xFD, 0xB1); // unlock commands
	display_command0(0xAE);// turn off display
	display_command1(0xB3, 0xA0); // set clock div
	display_command1(0xCA, 0x7F); // set mux ratio
	display_command1(0xA2, 0x00); // set offset
	display_command1(0xA1, 0x00); // set start line
	display_command1(0xA0, 0b00110000); // set remap (0b00110000 to flip display verticaly)
	display_command1(0xB5, 0x00); // set gpio
	display_command1(0xAB, 0x01); // set function select
	display_command3(0xB4, 0xA0, 0xB5, 0x55); // set vsl
	display_command3(0xC1, 0xFF, 0xFF, 0xFF); // set contrast abc
	display_command1(0xC7, 0x0F); // set contrast master
	display_command0(0xB9); // use lut
	display_command1(0xB1, 0x32); // set precharge
	display_command1(0xBB, 0x00); // set precharge level
	display_command1(0xB6, 0x01); // set precharge 2
	display_command1(0xBE, 0x05); // set vcomh
	display_command3(0xB2, 0xA4, 0x00, 0x00); // set display enchancement
	display_command0(0xA6); // set normal display
    display_command0(0xAF); // turn on display

    waiting = false;
}

void display_on() {
	display_command0(0xAF);
}

void display_off() {
	display_command0(0xAE);
}

static void swap(uint16_t* a, uint16_t* b) {
    uint16_t tmp = *a;
    *a = *b;
    *b = tmp;
}

void display_send(RGB565* framebuffer) {
    uint16_t* data = (uint16_t*) framebuffer;

    // rotate
    // for (int x = 0; x < display_w; x++) {
    //     for (int y = 0; y < x; y++) {
    //         swap(&data[display_w * y + x], &data[display_w * x + y]);
    //     }
    // }

    // swap byte order
    for (int i = 0; i < display_w * display_h; i++) {
        data[i] = __builtin_bswap16(data[i]);
    }

    display_wait();

    display_command2(0x15, 0, display_w-1); // set column
    display_command2(0x75, 0, display_h-1); // set row
    display_command0(0x5C); // write ram

    // transaction is static because transfer is async
    waiting = true;
    static spi_transaction_t transaction = { .length = display_w * display_h * 16 };
    transaction.tx_buffer = data;
    ESP_ERROR_CHECK(spi_device_queue_trans(spi, &transaction, portMAX_DELAY));
}
