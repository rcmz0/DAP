#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"

#include "esp_vfs_fat.h"
#include "diskio_sdmmc.h"
#include "sdmmc_cmd.h"
#include "driver/sdmmc_host.h"

#include "pins.h"

// FATFS sdfs_data;
// FATFS* sdfs = &sdfs_data;

bool sd_init() {
    sdmmc_host_t host_config = SDMMC_HOST_DEFAULT();
    // host_config.max_freq_khz = SDMMC_FREQ_PROBING;
    // host_config.flags = SDMMC_HOST_FLAG_1BIT;

    sdmmc_slot_config_t slot_config = SDMMC_SLOT_CONFIG_DEFAULT();
    slot_config.flags |= SDMMC_SLOT_FLAG_INTERNAL_PULLUP;
    slot_config.width = 1;
    slot_config.clk = SD_CLK_PIN;
    slot_config.cmd = SD_CMD_PIN;
    slot_config.d0 = SD_D0_PIN;
    slot_config.d1 = SD_D1_PIN;
    slot_config.d2 = SD_D2_PIN;
    slot_config.d3 = SD_D3_PIN;

    esp_vfs_fat_mount_config_t mount_config = {
        .max_files = 16,
    };

    sdmmc_card_t* card;
    return esp_vfs_fat_sdmmc_mount("/sd", &host_config, &slot_config, &mount_config, &card) != ESP_OK;

    // sdmmc_card_print_info(stdout, card);

    // ESP_ERROR_CHECK(esp_vfs_fat_register("/sd", "1", 16, &sdfs));
    // ff_diskio_register_sdmmc(
    // ESP_ERROR_CHECK(ffdiskio
}
