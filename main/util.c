#include "util.h"

int min(int a, int b) {
	return a<b ? a : b;
}

float minf(float a, float b) {
	return a<b ? a : b;
}

int max(int a, int b) {
	return a>b ? a : b;
}

float maxf(float a, float b) {
	return a>b ? a : b;
}

int clamp(int v, int a, int b) {
	return min(max(v, a), b);
}

float clampf(float v, float a, float b) {
	return minf(maxf(v, a), b);
}

int mod(int a, int b) {
	return a % b < 0 ? a % b + b : a % b;	
}

float maprangef(float value, float from_min, float from_max, float to_min, float to_max) {
	return (value - from_min) / (from_max - from_min) * (to_max - to_min) + to_min;
}
