#pragma once

#include "canvas.h"

void image_cache_init();
RGB565* image_cache_get(char* path);
