#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "esp_timer.h"
#include "driver/gpio.h"

#include "button.h"

void button_init(button_t* button, uint gpio) {
    button->gpio = gpio;
    button->down = false;

	gpio_set_direction(button->gpio, GPIO_MODE_INPUT);
    gpio_pullup_en(button->gpio);
}

bool button_get_pressed(button_t* button) {
    if (!gpio_get_level(button->gpio)) {
        if (!button->down) {
            button->down = true;
            return true;
        }
    } else {
        button->down = false;
    }

    return false;
}
