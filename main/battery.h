#pragma once

#include "stdbool.h"

void battery_init();
float battery_get_voltage();
int battery_get_percentage();
bool battery_is_charging();
