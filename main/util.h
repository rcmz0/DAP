#pragma once

int min(int a, int b);
float minf(float a, float b);
int max(int a, int b);
float maxf(float a, float b);
int clamp(int v, int a, int b);
float clampf(float v, float a, float b);
int mod(int a, int b);
float maprangef(float value, float from_min, float from_max, float to_min, float to_max);
