#pragma once

#include "stdbool.h"

typedef struct audio_info_t {
    char path[256];
    char codec[16];
    int bitrate;
    int channels;
    int depth;
    int samplerate;
    float position;
    float duration;
} audio_info_t;

void audio_init();
void audio_play(char* path);
void audio_previous();
void audio_next();
bool audio_is_paused();
void audio_pause();
void audio_resume();
void audio_toggle();
int audio_get_volume();
void audio_set_volume(int volume);
void audio_offset_volume(int offset);
audio_info_t* audio_get_info();
