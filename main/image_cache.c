#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "ff.h"

#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_timer.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image_resize.h"

#include "canvas.h"
#include "util.h"

static const char* default_image = "0:/snakeoil.png";

#define image_cache_size 16
static struct {
    char path[256];
    RGB565* image;
} image_cache[image_cache_size];

static QueueHandle_t image_queue;
static TaskHandle_t image_task;

static int stbi_read_callback(void* user, char* data, int size) {
	FIL* file = user;
	size_t bytes_read;
	f_read(file, data, size, &bytes_read);
	return bytes_read;
}

static void stbi_skip_callback(void* user, int n) {
	FIL* file = user;
	f_lseek(file, f_tell(file) + n);
}

static int eof_callback(void* user) {
	FIL* file = user;
	return f_eof(file);
}

static const stbi_io_callbacks stbi_callbacks = {
	.read = stbi_read_callback,
	.skip = stbi_skip_callback,
	.eof = eof_callback,
};

static RGB565* load_image(char* path) {
	// int64_t start_time = esp_timer_get_time();
	// int64_t end_time;

	FIL file;
	f_open(&file, path, FA_READ);

	int source_w, source_h, source_c;
	// stbi_info_from_callbacks(&callbacks, &file, &source_w, &source_h, &source_c);
	// printf("image:%s size:%u w:%i h:%i c:%i\n", path, f_size(&file), source_w, source_h, source_c);
	// f_rewind(&file);
	uint8_t* source = stbi_load_from_callbacks(&stbi_callbacks, &file, &source_w, &source_h, &source_c, 3);
	f_close(&file);

	if (!source) {
		printf("stbi failed: %s\n", stbi_failure_reason());
		return NULL;
	}

	// end_time = esp_timer_get_time();
	// printf("image load time : %f\n", (float) (end_time - start_time)/1000);
	// start_time = end_time;

	if (source_w != display_w || source_h != display_h) {
		uint8_t *resized = malloc(display_w * display_h * 3);
		stbir_resize_uint8(source, source_w, source_h, 0, resized, display_w, display_h, 0, 3);
		free(source);
		source = resized;
	}

	// // end_time = esp_timer_get_time();
	// // printf("image resize time : %f\n", (float) (end_time - start_time)/1000);
	// // start_time = end_time;

	RGB565 *image = malloc(display_w * display_h * 2);
	float darken = 0.5;
	for (int i = 0; i < display_w*display_h; i++) {
		image[i] = (RGB565) {
			(uint8_t)(source[i*3+0] * darken) >> 3,
			(uint8_t)(source[i*3+1] * darken) >> 2,
			(uint8_t)(source[i*3+2] * darken) >> 3,
		};
	}
	free(source);

	// end_time = esp_timer_get_time();
	// printf("image transcode time : %f\n", (float) (end_time - start_time)/1000);
	// start_time = end_time;
	
	return image;
}

static void image_task_function(void* arg) {
	while (true) {
		int i;
		xQueueReceive(image_queue, &i, portMAX_DELAY);
		image_cache[i].image = load_image(image_cache[i].path);
	}
}

void image_cache_init() {
	memset(image_cache, 0, sizeof(image_cache));
	image_queue = xQueueCreate(3, sizeof(int));
	assert(xTaskCreate(image_task_function, "image", 16*1024, NULL, 1, &image_task) == pdPASS);
}

RGB565* image_cache_get(const char* path) {
	if (!path || !*path) path = default_image;

	for (int i = 0; i < image_cache_size; i++) {
		if (!strcmp(image_cache[i].path, path)) {
			return image_cache[i].image;
		}
	}

	int i;

	for (i = 0; i < image_cache_size; i++) {
		if (!*image_cache[i].path) {
			break;
		}
	}

	if (i == image_cache_size) {
		i = rand() % image_cache_size;
		*image_cache[i].path = 0;
        free(image_cache[i].image);
	}

	strcpy(image_cache[i].path, path);
	image_cache[i].image = NULL;
	xQueueSend(image_queue, &i, portMAX_DELAY);

    return image_cache[i].image;
};
