#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "stdbool.h"

#include "math.h"

#include "screen.h"
#include "canvas.h"
#include "util.h"

static struct {
	screen_t* screen;
	animation_t animation;
} stack[16];
static int stack_length = 0;

animation_t animation = ANIMATION_NONE;
static float animation_tween = 0;
static float animation_tween_goal = 0;

static float crt_tween = 0;
static float crt_tween_goal = 0;

static RGB565 topscreen_framebuffer[display_w*display_h];
static RGB565 animation_framebuffer[display_w*display_h];

screen_t* screen_get_top() {
	return stack[stack_length-1].screen;
}

static void screen_draw_internal() {
    screen_get_top()->draw(screen_get_top());
}

void screen_wheel(int offset) {
    screen_get_top()->wheel(screen_get_top(), offset);
}

void screen_select() {
    screen_get_top()->select(screen_get_top());
}

void screen_back() {
    screen_get_top()->back(screen_get_top());
}

void screen_playing() {
    screen_get_top()->playing(screen_get_top());
}

void screen_previous() {
    screen_get_top()->previous(screen_get_top());
}

void screen_next() {
    screen_get_top()->next(screen_get_top());
}

static void screen_delete_internal() {
    screen_get_top()->delete(screen_get_top());
}

static void save_topscreen() {
	canvas_set_framebuffer(animation_framebuffer);
	canvas_clear();
	if (stack_length) screen_draw_internal();
}

void screen_replace(screen_t* screen, animation_t _animation) {
	save_topscreen();
	animation = _animation;
	animation_tween_goal = 1;
	animation_tween = 0;

	stack[stack_length-1].screen = screen;

	canvas_clear_scrolls();
}

void screen_push(screen_t* screen, animation_t _animation) {
	save_topscreen();
	animation = _animation;
	animation_tween_goal = 1;
	animation_tween = 0;

	stack[stack_length].screen = screen;
	stack[stack_length].animation = animation;
	stack_length++;

	canvas_clear_scrolls();
}

void screen_pop() {
	if (stack_length <= 1) return;

	save_topscreen();
	animation = stack[stack_length-1].animation;
	animation_tween_goal = -1;
	animation_tween = 0;

	screen_delete_internal();
	stack_length--;

	canvas_clear_scrolls();
}

void screen_toggle() {
	crt_tween_goal = 1 - crt_tween_goal;
}

void screen_draw(RGB565* framebuffer) {
	animation_tween += (animation_tween_goal - animation_tween) * 10 * 1/50;
	if (fabsf(animation_tween_goal - animation_tween) < (float) 1/display_w) animation_tween = animation_tween_goal;

	crt_tween += (crt_tween_goal - crt_tween) * 20 * 1/50;
	if (fabsf(crt_tween_goal - crt_tween) < (float) 1/display_h) crt_tween = crt_tween_goal;

	bool x = (animation == ANIMATION_LEFT) || (animation == ANIMATION_RIGHT);
	bool y = (animation == ANIMATION_TOP) || (animation == ANIMATION_BOTTOM);
	int dir = ((animation == ANIMATION_LEFT) || (animation == ANIMATION_TOP))
		- ((animation == ANIMATION_RIGHT) || (animation == ANIMATION_BOTTOM));

	float topscreen_x = (animation_tween - animation_tween_goal) * x * dir;
	float topscreen_y = (animation_tween - animation_tween_goal) * y * dir;
	float animation_x = animation_tween * x * dir;
	float animation_y = animation_tween * y * dir;

	if (fabsf(animation_x) != 1 || fabsf(animation_y) != 1) {
		canvas_set_framebuffer(animation_framebuffer);
		canvas_copy(framebuffer, roundf(animation_x * display_w), roundf(animation_y * display_h));
	}

	canvas_set_framebuffer(topscreen_framebuffer);
	canvas_clear();
	screen_draw_internal();
	canvas_copy(framebuffer, roundf(topscreen_x * display_w), roundf(topscreen_y * display_h));

	if (crt_tween < 1) {
		canvas_set_framebuffer(framebuffer);

		int crt_window_height = roundf(crt_tween * display_h/2);
		int crt_black_height = display_h/2 - crt_window_height;

		for (int half = 0; half <= 1; half++) {
			for (int y = 0; y < crt_window_height; y++) {
				int py, bin_start, bin_end;

				if (half == 0) {
					py = display_h/2-1 - y;
					bin_start = display_h/2-1 - (y+1) / crt_tween + 1;
					bin_end = display_h/2-1 - y / crt_tween;
				} else {
					py = display_h/2 + y;
					bin_start = display_h/2 + y / crt_tween;
					bin_end = display_h/2 + (y+1) / crt_tween - 1;
				}

				float brightness_factor = 1 / crt_tween / (bin_end - bin_start + 1);

				for (int px = 0; px < display_w; px++) {
					int bin_r = 0;
					int bin_g = 0;
					int bin_b = 0;

					for (int by = bin_start; by <= bin_end; by++) {
						RGB565 p = framebuffer[display_w * by + px];
						bin_r += p.r;
						bin_g += p.g;
						bin_b += p.b;
					}

					RGB565 bin;
					bin.r = min(bin_r * brightness_factor, 0x1f);
					bin.g = min(bin_g * brightness_factor, 0x3f);
					bin.b = min(bin_b * brightness_factor, 0x1f);
					framebuffer[display_w * py + px] = bin;
				}
			}

			if (half == 0) {
				canvas_draw_rectangle(0, 0, display_w, crt_black_height, 0x00);
			} else {
				canvas_draw_rectangle(0, display_h - crt_black_height, display_w, crt_black_height, 0x00);
			}
		}
	}
}
