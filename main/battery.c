#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "math.h"

#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc/adc_oneshot.h"
#include "esp_adc/adc_continuous.h"
#include "esp_adc/adc_cali.h"
#include "esp_adc/adc_cali_scheme.h"

#include "util.h"

static const int unit = ADC_UNIT_1;
static const int channel = ADC1_CHANNEL_9;
static const int attenuation = ADC_ATTEN_DB_2_5;
static const int bitwidth = ADC_BITWIDTH_DEFAULT;
static const int charging_pin = 33;

static adc_oneshot_unit_handle_t handle;
static adc_cali_handle_t calibration_handle;

#define battery_samples_num 1024
static float samples[battery_samples_num];
static float samples_sum = 0;
static int samples_index = 0;

float battery_get_voltage_internal() {
    const int voltage_divider_r1 = 442;
    const int voltage_divider_r2 = 160;
    const float voltage_divider = (float) voltage_divider_r2 / (voltage_divider_r1 + voltage_divider_r2);

    int raw;
    ESP_ERROR_CHECK(adc_oneshot_read(handle, channel, &raw));
    int voltage;
    ESP_ERROR_CHECK(adc_cali_raw_to_voltage(calibration_handle, raw, &voltage));
    return voltage / voltage_divider / 1000;
}

void battery_init() {
    adc_oneshot_unit_init_cfg_t unit_config = {
        .unit_id = unit,
        .ulp_mode = ADC_ULP_MODE_DISABLE,
    };
    ESP_ERROR_CHECK(adc_oneshot_new_unit(&unit_config, &handle));

    adc_oneshot_chan_cfg_t channel_config = {
        .atten = attenuation,
        .bitwidth = bitwidth,
    };
    ESP_ERROR_CHECK(adc_oneshot_config_channel(handle, channel, &channel_config));

    adc_cali_curve_fitting_config_t calibration_config = {
        .unit_id = unit,
        .atten = attenuation,
        .bitwidth = bitwidth,
    };
    ESP_ERROR_CHECK(adc_cali_create_scheme_curve_fitting(&calibration_config, &calibration_handle));

    // adc_continuous_handle_cfg_t adc_handle_config = {
    //     .max_store_buf_size = 1024,
    //     .conv_frame_size = 256,
    // };

    // adc_continuous_handle_t adc_handle;

    // ESP_ERROR_CHECK(adc_continuous_new_handle(&adc_handle_config, &adc_handle));

    // adc_digi_pattern_config_t adc_pattern_config = {
    //     .channel = channel,
    //     .atten = attenuation,
    //     .unit = ADC_UNIT_1,
    //     .bit_width = SOC_ADC_DIGI_MAX_BITWIDTH,
    // };

    // adc_continuous_config_t adc_config = {
    //     .pattern_num = 1,
    //     .adc_pattern = &adc_pattern_config,
    //     .sample_freq_hz = 1000,
    //     .conv_mode = ADC_CONV_SINGLE_UNIT_1,
    //     .format = ADC_DIGI_OUTPUT_FORMAT_TYPE2,
    // };

    // ESP_ERROR_CHECK(adc_continuous_config(&adc_handle, &adc_config));

    samples_sum = 0;
    for (int i = 0; i < battery_samples_num; i++) {
        samples[i] = battery_get_voltage_internal();
        samples_sum += samples[i];
    }

    gpio_set_direction(charging_pin, GPIO_MODE_INPUT);
    gpio_pullup_en(charging_pin);
}

float battery_get_voltage() {
    samples_sum -= samples[samples_index];
    samples[samples_index] = battery_get_voltage_internal();
    samples_sum += samples[samples_index];
    samples_index = (samples_index + 1) % battery_samples_num;
    return samples_sum / battery_samples_num;
}

int battery_get_percentage() {
    return roundf(maprangef(battery_get_voltage(), 3.2, 4.2, 0, 100));
}

bool battery_is_charging() {
    return gpio_get_level(charging_pin);
}
