#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "stdarg.h"
#include "math.h"

#include "utf8.h"

#include "util.h"
#include "canvas.h"
#include "display.h"
#include "font.h"
#include "image_cache.h"

static RGB565* framebuffer;

typedef struct scroll_t {
	char text[256];
	int width;
	int marker;
	int wait;
	bool reset;
	float tween;
	float tween_goal;
} scroll_t;

#define text_scroll_cache_size 16
static scroll_t scroll_cache[text_scroll_cache_size] = {};
static const int scroll_wait = 50;
static const float scroll_speed = 0.5;

void canvas_set_framebuffer(RGB565* _framebuffer) {
	framebuffer = _framebuffer;
}

void canvas_copy(RGB565* target, int x, int y) {
	for (int py = max(y, 0); py < min(y+display_h, display_h); py++) {
		for (int px = max(x, 0); px < min(x+display_w, display_w); px++) {
			target[display_w * py + px] = framebuffer[display_w * (py - y) + (px - x)];
		}
	}
}

void canvas_clear() {
	memset(framebuffer, 0, display_w*display_h*2);
}

void canvas_add_image(char* path) {
	RGB565* image = image_cache_get(path);
	if (!image) return;

	for (int py = 0; py < display_h; py++) {
		for (int px = 0; px < display_w; px++) {
			int i = display_w * py + px;
			RGB565* p = &framebuffer[i];
			RGB565* v = &image[i];

			p->r = min(p->r + v->r, 0x1f);
			p->g = min(p->g + v->g, 0x3f);
			p->b = min(p->b + v->b, 0x1f);
		}
	}
}

void canvas_draw_rectangle(int x, int y, int w, int h, uint8_t v) {
	for (int py = y; py < y+h; py++) {
		for (int px = x; px < x+w; px++) {
			RGB565* p = &framebuffer[display_w * py + px];
			p->r = v >> 3;
			p->g = v >> 2;
			p->b = v >> 3;
		}
	}
}

void canvas_invert_rectangle(int x, int y, int w, int h) {
	for (int py = y; py < y+h; py++) {
		for (int px = x; px < x+w; px++) {
			int16_t* p = (int16_t*) &framebuffer[display_w * py + px];
			*p ^= 0xffff;
		}
	}
}

void canvas_clear_scrolls() {
	for (int i = 0; i < text_scroll_cache_size; i++) {
		scroll_cache[i].tween = 0;
		scroll_cache[i].tween_goal = 0;
		scroll_cache[i].reset = false;
		scroll_cache[i].wait = scroll_wait;
	}
}

static scroll_t* scroll_cache_get(char* text) {
	int i;

	for (i = 0; i < text_scroll_cache_size; i++) {
		if (!strcmp(scroll_cache[i].text, text)) {
			break;
		}
	}

	if (i == text_scroll_cache_size) {
		int min_marker = INFINITY;
		for (int j = 0; j < text_scroll_cache_size; j++) {
			if (scroll_cache[j].marker < min_marker) {
				min_marker = scroll_cache[j].marker;
				i = j;
			}
		}

		strcpy(scroll_cache[i].text, text);
		scroll_cache[i].width = font_get_text_width(text);
		scroll_cache[i].tween = 0;
		scroll_cache[i].tween_goal = 0;
		scroll_cache[i].reset = false;
		scroll_cache[i].wait = scroll_wait;
	}

	static int marker = 0;
	marker++;
	scroll_cache[i].marker = marker;

	return &scroll_cache[i];
}

float digit_cache_get(int x, int y, int i) {
	static struct {
		int x;
		int y;
		float f;
	} cache[20] = {};
	const int cache_len = 20;

	int c = 0;

	for (c = 0; c < cache_len; c++) {
		if (cache[c].x == x && cache[c].y == y) {
			if (i == 0 && cache[c].f > 5) cache[c].f -= 10;
			if (i == 9 && cache[c].f < 5) cache[c].f += 10;
			cache[c].f += (i - cache[c].f) * 10 * 1/50;
			break;
		}
	}

	if (c == cache_len) {
		for (c = 0; c < cache_len; c++) {
			if (cache[c].x == 0 && cache[c].y == 0) {
				break;
			}
		}

		if (c == cache_len) {
			c = rand() % cache_len;
		}

		cache[c].x = x;
		cache[c].y = y;
		cache[c].f = i;
	}

	return cache[c].f;
}

static void draw_digit(int x, int y, int offset, int i) {
	const int line_h = 16;

	glyph_t *glyph = font_get_glyph('0' + i % 10);

	for (int gy = 0; gy < glyph->height; gy++) {
		for (int gx = 0; gx < glyph->width; gx++) {
			int px = x + glyph->offset_x + gx;
			int py = y + glyph->offset_y + gy + offset;
			if (py < y || py >= y+line_h) continue;
			if (px < 0 || px >= display_w || py < 0 || py >= display_h) continue;

			uint8_t v = glyph->bitmap[glyph->width * gy + gx];
			if (v == 0) continue;

			RGB565* p = &framebuffer[display_w * py + px];
			p->r = v >> 3;
			p->g = v >> 2;
			p->b = v >> 3;
		}
	}
}

void draw_animated_digit(int x, int y, int i) {
	const int line_h = 16;
	float f = digit_cache_get(x, y, i);

	for (int o = -3; o <= 3; o++) {
		draw_digit(x, y, (i-f+o)*line_h, i+o);
	}
}

static void draw_text(int x, int y, char* text, float l) {
	bool tag_no_scroll = *text == ':';
	if (tag_no_scroll) text++;

	scroll_t* scroll = scroll_cache_get(text);

	if (tag_no_scroll) {
		scroll->reset = true;
	}

	if (scroll->width > display_w) {
		if (scroll->wait) {
			scroll->wait--;
		} else {
			if (scroll->reset) {
				scroll->reset = false;
				scroll->tween_goal = 0;
				scroll->wait = scroll_wait;
			} else {
				scroll->tween_goal += scroll_speed;
				if (-scroll->tween + scroll->width < display_w) {
					scroll->wait = scroll_wait;
					scroll->reset = true;
				}
			}
		}
	}

	if (scroll->width > display_w) {
		x = roundf(-scroll->tween);
		scroll->tween += (scroll->tween_goal - scroll->tween) * 10 * 1/50;
		if (fabsf(scroll->tween_goal - scroll->tween) < 1) scroll->tween = scroll->tween_goal;
	}

	while (true) {
		int codepoint;
		text = utf8codepoint(text, &codepoint);
		if (codepoint == 0) break;

		// if (codepoint == '\n') {
		// 	y += 20;
		// 	x = x;
		// 	continue;
		// }

		// if (codepoint >= '0' && codepoint <= '9') {
		// 	int i = codepoint - '0';
		// 	draw_animated_digit(x, y, i);
		// 	x += 8;
		// 	continue;
		// } 

		glyph_t* glyph = font_get_glyph(codepoint);

		for (int gy = 0; gy < glyph->height; gy++) {
			for (int gx = 0; gx < glyph->width; gx++) {
				int px = x + glyph->offset_x + gx;
				int py = y + glyph->offset_y + gy;
				if (px < 0 || px >= display_w || py < 0 || py >= display_h) continue;

				uint8_t v = glyph->bitmap[glyph->width * gy + gx];
				// v *= l;
				if (v == 0) continue;

				RGB565* p = &framebuffer[display_w * py + px];
				// p->r = v >> 3;
				// p->g = v >> 2;
				// p->b = v >> 3;
				p->r = ((uint8_t)(v * clampf((1-l)*2, 0, 1))) >> 3;
				p->g = ((uint8_t)(v * clampf(    l*2, 0, 1))) >> 2;
				p->b = ((uint8_t)(v * clampf(    l*2, 0, 1))) >> 3;
			}
		}

		x += glyph->advance;
	}
}

void canvas_draw_text(int x, int y, float l, char* format, ...) {
	char text[256];
	va_list args;
	va_start(args, format);
	vsprintf(text, format, args);
	va_end(args);
	draw_text(x, y, text, l);
}

void canvas_draw_text_left(int y, char* format, ...) {
	char text[256];
	va_list args;
	va_start(args, format);
	vsprintf(text, format, args);
	va_end(args);
	draw_text(0, y, text, 1);
}

void canvas_draw_text_center(int y, char* format, ...) {
	char text[256];
	va_list args;
	va_start(args, format);
	vsprintf(text, format, args);
	va_end(args);
	draw_text(roundf((display_w - font_get_text_width(text)) / 2.), y, text, 1);
}

void canvas_draw_text_right(int y, char* format, ...) {
	char text[256];
	va_list args;
	va_start(args, format);
	vsprintf(text, format, args);
	va_end(args);
	draw_text(display_w - font_get_text_width(text), y, text, 1);
}
