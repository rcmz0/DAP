#pragma once

typedef unsigned int uint;

typedef struct glyph_t {
	int codepoint;
	int offset_x;
	int offset_y;
	int width;
	int height;
	int advance;
	uint8_t* bitmap;
} glyph_t;

void font_init();
glyph_t* font_get_glyph(int codepoint);
int font_get_text_width(char* text);
