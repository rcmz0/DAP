#pragma once

#include "canvas.h"

#define display_w 128
#define display_h 128

void display_init();
void display_on();
void display_off();
void display_send(RGB565* framebuffer);
