#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdint.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_timer.h"
#include "driver/gpio.h"

#include "pins.h"
#include "display.h"
#include "flash.h"
#include "sd.h"
#include "battery.h"
#include "canvas.h"
#include "font.h"
#include "image_cache.h"
#include "rotary.h"
#include "button.h"
#include "audio.h"
#include "directory_screen.h"
#include "playing_screen.h"
#include "keyboard_screen.h"
#include "performance_analyzer.h"

static RGB565 framebuffers[2][display_w*display_h] = {};
static RGB565* framebuffer = framebuffers[0];

void app_main() {
    // gpio_set_direction(LDO2_ENABLE_PIN, GPIO_MODE_OUTPUT);
    // gpio_set_level(LDO2_ENABLE_PIN, 1);

    display_init();
	display_send(framebuffer);

	audio_init();
	flash_init(); // flash path is 0:
    font_init();
    sd_init(); // sd path is 1:
	image_cache_init();
	battery_init();

	rotary_init();

    button_t select_button; button_init(&select_button, SELECT_BUTTON_PIN);
	button_t back_button; button_init(&back_button, BACK_BUTTON_PIN);
	button_t playing_button; button_init(&playing_button, PLAYING_BUTTON_PIN);
	button_t previous_button; button_init(&previous_button, PREVIOUS_BUTTON_PIN);
	button_t next_button; button_init(&next_button, NEXT_BUTTON_PIN);

	button_t volume_up_button; button_init(&volume_up_button, VOLUME_UP_BUTTON_PIN);
	button_t volume_down_button; button_init(&volume_down_button, VOLUME_DOWN_BUTTON_PIN);
	button_t power_button; button_init(&power_button, POWER_BUTTON_PIN);

    screen_push(directory_screen_new("1:"), ANIMATION_NONE);

    screen_push(keyboard_screen_get(), ANIMATION_NONE);

	screen_toggle();

    for (int frame = 0;; frame++) {
		// performance_analyzer_tick();

		int wheel_offset = rotary_get_offset();
		if (wheel_offset) screen_wheel(wheel_offset);

		if (button_get_pressed(&select_button)) screen_select();
		if (button_get_pressed(&back_button)) screen_back();
		if (button_get_pressed(&playing_button)) screen_playing();
		if (button_get_pressed(&previous_button)) screen_previous();
		if (button_get_pressed(&next_button)) screen_next();

		if (button_get_pressed(&volume_up_button)) audio_offset_volume(+1);
		if (button_get_pressed(&volume_down_button)) audio_offset_volume(-1);
		if (button_get_pressed(&power_button)) screen_toggle();

		screen_draw(framebuffer);
		display_send(framebuffer);
		framebuffer = framebuffer==framebuffers[0] ? framebuffers[1] : framebuffers[0];
    }
}
