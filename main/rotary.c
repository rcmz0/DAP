#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "driver/gpio.h"
#include "esp_timer.h"

#include "pins.h"
#include "rotary.h"

static const int transition_table[4][4] = {
	[0b00][0b01] = -1,
	[0b01][0b11] = -1,
	[0b11][0b10] = -1,
	[0b10][0b00] = -1,
	[0b00][0b10] = +1,
	[0b10][0b11] = +1,
	[0b11][0b01] = +1,
	[0b01][0b00] = +1,
};

static uint a = ROTARY_A_PIN;
static uint b = ROTARY_B_PIN;

static volatile int previous_state = 0;
static volatile int position = 0;
static volatile int previous_detent = 0;
static volatile int offset = 0;

static void rotary_gpio_callback(void* arg) {
	int state = (gpio_get_level(a) << 1) | (gpio_get_level(b));
	position += transition_table[previous_state][state];
	previous_state = state;

    int detent = position / 2;
	if (detent != previous_detent) {
        offset += detent - previous_detent;
        previous_detent = detent;
	}
}

void rotary_init() {
	gpio_set_direction(a, GPIO_MODE_INPUT);
	gpio_set_direction(b, GPIO_MODE_INPUT);
	gpio_pullup_en(a);
	gpio_pullup_en(b);
	gpio_set_intr_type(a, GPIO_INTR_ANYEDGE);
	gpio_set_intr_type(b, GPIO_INTR_ANYEDGE);
    gpio_install_isr_service(0);
	gpio_isr_handler_add(a, rotary_gpio_callback, NULL);
	gpio_isr_handler_add(b, rotary_gpio_callback, NULL);
}

int rotary_get_offset() {
	// if (offset) {
	// 	static int64_t last_call_time = 0;

	// 	int64_t call_time = esp_timer_get_time();
	// 	int time_between_calls = (call_time - last_call_time);

	// 	static float avg_time_between_calls = 0;
	// 	static int num_calls = 0;

	// 	if (last_call_time != 0) {
	// 		avg_time_between_calls = (avg_time_between_calls * num_calls + time_between_calls) / (num_calls + 1);
	// 		num_calls++;
	// 	}

	// 	last_call_time = call_time;
	// 	printf("avg time between %i rotary calls: %f ms\n", num_calls, avg_time_between_calls/1000);
	// }

	int res = offset;
	offset = 0;
	return res;
}
