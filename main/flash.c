#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "esp_vfs_fat.h"

void flash_init() {
	esp_vfs_fat_mount_config_t mount_config = {
		.max_files = 4,
		.format_if_mount_failed = false,
	};

	ESP_ERROR_CHECK(esp_vfs_fat_rawflash_mount("/flash", "storage", &mount_config));
}
