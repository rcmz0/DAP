#pragma once

#define LDO2_ENABLE_PIN 17

/* PCB */

#define ROTARY_A_PIN 36
#define ROTARY_B_PIN 34

#define SELECT_BUTTON_PIN 8
#define BACK_BUTTON_PIN 6
#define PLAYING_BUTTON_PIN 38
#define NEXT_BUTTON_PIN 37
#define PREVIOUS_BUTTON_PIN 35

#define VOLUME_UP_BUTTON_PIN 39
#define VOLUME_DOWN_BUTTON_PIN 9
#define POWER_BUTTON_PIN 12

#define DISPLAY_SCK_PIN 16
#define DISPLAY_MOSI_PIN 15
#define DISPLAY_RST_PIN 14
#define DISPLAY_DC_PIN 13

#define SD_CLK_PIN 4
#define SD_CMD_PIN 3
#define SD_D0_PIN 5
#define SD_D1_PIN 21
#define SD_D2_PIN 1
#define SD_D3_PIN 2

#define I2S_OUT_PIN 41
#define I2S_BCK_PIN 42
#define I2S_WS_PIN 40

/* BREADBOARD */

// #define ROTARY_A_PIN 1
// #define ROTARY_B_PIN 2

// #define SELECT_BUTTON_PIN 3
// #define BACK_BUTTON_PIN 4
// #define PLAYING_BUTTON_PIN 5
// #define NEXT_BUTTON_PIN 21
// #define PREVIOUS_BUTTON_PIN 0

// #define VOLUME_UP_BUTTON_PIN 38
// #define VOLUME_DOWN_BUTTON_PIN 39
// #define POWER_BUTTON_PIN 16

// #define DISPLAY_SCK_PIN 15
// #define DISPLAY_MOSI_PIN 14
// #define DISPLAY_RST_PIN 13
// #define DISPLAY_DC_PIN 12

// #define SD_CLK_PIN 7
// #define SD_CMD_PIN 9
// #define SD_D0_PIN 8
// #define SD_D1_PIN 36
// #define SD_D2_PIN 37
// #define SD_D3_PIN 34

// #define I2S_OUT_PIN 40
// #define I2S_BCK_PIN 41
// #define I2S_WS_PIN 42
