#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "math.h"

#include "keyboard_screen.h"
#include "canvas.h"
#include "font.h"
#include "util.h"

const char glyphs[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!?@#$%%^&|~*+=-_,.;:'\"<>()[]{}";
const int glyph_count = sizeof(glyphs) - 1;
// const float radius = 0.45;
const int selector_w = 12;

int selector = 0;
float selector_tween = 0;

char text[64] = {};
int text_pos = 0;

void add_text(char glyph) {
    if (text_pos < sizeof(text))
        text[text_pos++] = glyph;
}

void remove_text() {
    if (text_pos > 0)
        text[--text_pos] = 0;
}

float get_r(float t) {
    return display_w * 0.8 * (0.1 - sin(t));
};

int get_x(float f) {
    return roundf(display_w * 0.5 + get_r(f) * cos(f));
}

int get_y(float f) {
    return roundf(display_h * 0.95 + get_r(f) * sin(f));
}

static void keyboard_screen_draw(screen_t* screen) {
    for (int i = -glyph_count/2; i < glyph_count/2; i++) {
        char glyph[] = { glyphs[mod(roundf(selector_tween) + i + 10, glyph_count)], 0 };
        float f = ((roundf(selector_tween) + i - selector_tween) / glyph_count - 0.1) * M_PI*2;
        canvas_draw_text(
            roundf(get_x(f) - font_get_text_width(glyph)/2.), 
            roundf(get_y(f) - 8), 
            (cos(f) + 1) / 2,
            // (cos(f) + 1.5) / 2.5,
            glyph
        );
    }

    float f = -M_PI/2;
    canvas_invert_rectangle(
        roundf(get_x(f) - selector_w/2),
        roundf(get_y(f) - selector_w/2),
        selector_w, selector_w    
    );

    canvas_draw_text(roundf((display_w - font_get_text_width(text)) / 2.), display_h/2, 0.5, text);

    // canvas_add_image(NULL);

    selector_tween += (selector - selector_tween) * 20 * 1/50;
}

static void keyboard_screen_wheel(screen_t* screen, int offset) {
    selector += offset;
}

static void keyboard_screen_select(screen_t* screen) {
    add_text(glyphs[mod(selector, glyph_count)]);
}

static void keyboard_screen_back(screen_t* screen) {
}

static void keyboard_screen_playing(screen_t* screen) {
}

static void keyboard_screen_previous(screen_t* screen) {
    remove_text();
}

static void keyboard_screen_next(screen_t* screen) {
    add_text(' ');
}

static void keyboard_screen_delete(screen_t* screen) {
}

screen_t* keyboard_screen_get() {
    static screen_t keyboard_screen = {
        .draw = keyboard_screen_draw,
        .wheel = keyboard_screen_wheel,
        .select = keyboard_screen_select,
        .back = keyboard_screen_back,
        .playing = keyboard_screen_playing,
        .previous = keyboard_screen_previous,
        .next = keyboard_screen_next,
        .delete = keyboard_screen_delete,
    };
    return &keyboard_screen;
}
