#pragma once

typedef unsigned int uint;

typedef struct button_t {
    uint gpio;
    bool down;
} button_t;

void button_init(button_t* button, uint gpio);
bool button_get_pressed(button_t* button);
