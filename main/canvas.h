#pragma once

typedef struct RGB565 {
	uint16_t r : 5;
	uint16_t g : 6;
	uint16_t b : 5;
} RGB565;

#include "display.h"

void canvas_set_framebuffer(RGB565* framebuffer);
void canvas_copy(RGB565* target, int x, int y);

void canvas_clear();
void canvas_add_image(char* path);
void canvas_draw_rectangle(int x, int y, int w, int h, uint8_t v);
void canvas_invert_rectangle(int x, int y, int w, int h);
void canvas_draw_text(int x, int y, float l, char* format, ...);
void canvas_draw_text_left(int y, char* format, ...);
void canvas_draw_text_center(int y, char* format, ...);
void canvas_draw_text_right(int y, char* format, ...);
void canvas_clear_scrolls();
