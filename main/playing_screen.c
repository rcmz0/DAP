#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "math.h"

#include "playing_screen.h"
#include "canvas.h"
#include "audio.h"
#include "battery.h"
#include "util.h"

static const int line_h = 16;
static const int seekbar_h = 2;

static const int status_y = 0;
static const int seekbar_y = (display_h - seekbar_h);
static const int time_y = (seekbar_y - line_h-4);
// static const int quality_y = (time_y - line_h*2);
// static const int file_y = (((status_y+line_h+quality_y) - line_h*3)/2);
static const int file_y = (((status_y+line_h+time_y) - line_h*3)/2);

static void playing_screen_draw(screen_t* screen) {
    audio_info_t* info = audio_get_info();

    // volume 
    int volume = audio_get_volume();
    char* volume_icon = ((char*[]) { "", "", "", "" })[clamp(volume * 4 / 100, 0, 3)];
    canvas_draw_text_left(status_y, "%i %s", volume, volume_icon);

    // custom icon
    // canvas_draw_text_center(status_y, "");
    // canvas_draw_text_center(status_y, "");
    // canvas_draw_text_center(status_y, "");

    // battery
    int battery = battery_get_percentage();
    char* battery_icon = ((char*[]) { "", "", "", "", "" })[clamp(battery * 5 / 100, 0, 4)];
    if (battery_is_charging()) battery_icon = "";
    canvas_draw_text_right(status_y, "%s %i", battery_icon, battery);
    // canvas_draw_text_right(status_y+line_h, "%.2fV", battery_get_voltage());

    // track
    char path[256];
    strcpy(path, info->path);

    char* track = strrchr(path, '/');
    if (track) {
        *track = 0;
        track++;
        char* extension = strrchr(track, '.');
        if (extension) *extension = 0;
        char* index = strchr(track, '.');
        if (index) track = index+1;
        canvas_draw_text_center(file_y+line_h*0, " %s", track);

        char* album = strrchr(path, '/');
        if (album && album[2] != ':') {
            *album = 0;
            album++;
            canvas_draw_text_center(file_y+line_h*1, " %s", album);

            char* artist = strrchr(path, '/');
            if (artist && artist[2] != ':') {
                *artist = 0;
                artist++;
                canvas_draw_text_center(file_y+line_h*2, " %s", artist);
            }
        }
    }

    // quality
    // canvas_draw_text_center(quality_y, "%s %iKBPS", info->codec, info->bitrate);
    // canvas_draw_text_center(quality_y+line_h, "%iBIT %iHZ", info->depth, info->samplerate);

    // time
    canvas_draw_text_left(time_y, "%02i:%02i", (int)info->position/60, (int)info->position%60);
    // canvas_draw_text_center(time_y, audio_is_paused() ? "" : "");
    canvas_draw_text_right(time_y, "%02i:%02i", (int)info->duration/60, (int)info->duration%60);

    // seekbar
    if (info->duration) {
        float seek = (float) info->position/info->duration;
        canvas_draw_rectangle(0, seekbar_y, seek*display_w, seekbar_h, 0xff);
    }

    // visualizer
    // static float time = 0;
    // time += 1./50.;
    // for (int i = 0; i < 15; i++) {
    //     int h = 10
    //         * maprangef(sin(time * 10.3 + i * 2.1), -1, 1, 0.6, 1)
    //         * maprangef(sin(time *  5.2 - i * 1.1), -1, 1, 0.6, 1)
    //         * maprangef(sin(time *  1.7 + i * 0.3), -1, 1, 0.6, 1);
    //     canvas_draw_rectangle(35 + i * 4, time_y + 13 - h, 2, h, 0xff);
    // }

    // image
    canvas_add_image(screen->background);
}

static void playing_screen_wheel(screen_t* screen, int offset) {
    audio_offset_volume(offset);
}

static void playing_screen_select(screen_t* screen) {
    audio_toggle();
}

static void playing_screen_back(screen_t* screen) {
    screen_pop();
}

static void playing_screen_playing(screen_t* screen) {
    screen_pop();
}

static void playing_screen_previous(screen_t* screen) {
    audio_previous();
}

static void playing_screen_next(screen_t* screen) {
    audio_next();
}

static void playing_screen_delete(screen_t* screen) {
}

screen_t* playing_screen_get() {
    static screen_t playing_screen = {
        .draw = playing_screen_draw,
        .wheel = playing_screen_wheel,
        .select = playing_screen_select,
        .back = playing_screen_back,
        .playing = playing_screen_playing,
        .previous = playing_screen_previous,
        .next = playing_screen_next,
        .delete = playing_screen_delete,
    };

    return &playing_screen;
}
