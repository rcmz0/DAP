#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "ff.h"
#include "math.h"
#include "dirent.h"
#include "sys/stat.h"

#include "directory_screen.h"
#include "playing_screen.h"
#include "canvas.h"
#include "font.h"
#include "image_cache.h"
#include "audio.h"
#include "sd.h"
#include "util.h"

static const int line_h = 16;
static const int line_num = display_h/line_h - 1;

typedef struct directory_screen_t {
	char path[256];

    char* entry_buffer;
    char** entries;
    int num_entries;

    int top;
    float top_tween;

    int selector;
    float selector_tween;

    bool invalid;
} directory_screen_t;

static void directory_screen_open(screen_t* screen) {
    directory_screen_t* self = screen->self;

    FF_DIR dir;
    self->invalid = f_opendir(&dir, self->path);
    if (self->invalid) return;

    FILINFO entry;
    int entry_buffer_length = 0;
    char background[256] = {};

    while(true) {
        f_readdir(&dir, &entry);
        if (!*entry.fname) break;
        if (*entry.fname == '.') continue;

        if (!(entry.fattrib & AM_DIR)) {
            char* extension = strrchr(entry.fname, '.');

            if (!strcmp(extension, ".png") || (!strcmp(extension, ".jpg") && !*background)) {
                strcpy(background, entry.fname);
                continue;
            }

            if (strcmp(extension, ".mp3")) {
                continue;
            }
        }

        int entry_length = strlen(entry.fname)+1;
        int entry_offset = entry_buffer_length;
        entry_buffer_length += entry_length;
        self->num_entries++;

        self->entry_buffer = realloc(self->entry_buffer, entry_buffer_length);
        self->entries = realloc(self->entries, self->num_entries * sizeof(char*));

        memcpy(self->entry_buffer + entry_offset, entry.fname, entry_length);
        self->entries[self->num_entries-1] = (char*) entry_offset;
    }

    f_closedir(&dir);

    for (int i = 0; i < self->num_entries; i++) {
        self->entries[i] = self->entry_buffer + (int) self->entries[i];
    }

    if (*background) {
        strcat(screen->background, self->path);
        strcat(screen->background, "/");
        strcat(screen->background, background);
    }
}

static void directory_screen_draw(screen_t* screen) {
    directory_screen_t* self = screen->self;

    if (self->invalid) directory_screen_open(screen);

    // invalid
    if (self->invalid) {
        canvas_draw_text_center(display_h/2 - line_h/2, "No SD card detected");
        canvas_add_image(NULL);
        return;
    }

    // entries
    for (int i = floorf(self->top_tween); i < min(ceilf(self->top_tween)+line_num, self->num_entries); i++) {
        canvas_draw_text_left(
            roundf(line_h + (i - self->top_tween) * line_h),
            (i == self->selector) ? "%s" : ":%s",
            self->entries[i]
        );
    }

	// directory
	char* directory = strrchr(self->path, '/');
    char* icon = "";
    if (directory) {
        directory++;
    } else {
        directory = self->path;
        if (!strcmp(directory, "1:")) {
            directory = "SD";
            icon = "";
        }
    }
	canvas_draw_rectangle(0, 0, display_w, line_h, 0x00);
	canvas_draw_text_center(0, "%s %s", icon, directory);

	// selector
    canvas_invert_rectangle(0, roundf(line_h+(self->selector_tween-self->top_tween)*line_h), display_w, line_h);

    // // scrollbar
    float scrollbar_factor = (float) line_num/self->num_entries;
	if (scrollbar_factor < 1) {
        int scrollbar_width = 2;
		float scrollbar_height = scrollbar_factor*line_num*line_h;
		float scrollbar_center = maprangef(self->top_tween/(self->num_entries-line_num), 0, 1, line_h+scrollbar_height/2, line_h+line_num*line_h-scrollbar_height/2);
		canvas_invert_rectangle(display_w-scrollbar_width, roundf(scrollbar_center-scrollbar_height/2), scrollbar_width, scrollbar_height);
	}

    // image
    canvas_add_image(screen->background);

    // // update tweens
    self->top_tween += (self->top - self->top_tween) * 10 * 1/50;
    self->selector_tween += (self->selector - self->selector_tween) * 10 * 1/50;
}

static void directory_screen_wheel(screen_t* screen, int offset) {
    directory_screen_t* self = screen->self;

    if (self->invalid) return;

    int previous_selector = self->selector;
    self->selector = clamp(self->selector+offset, 0, self->num_entries-1);

    if (self->selector < self->top) {
        self->top = self->selector;
    }

    if (self->selector >= self->top+line_num) {
        self->top = self->selector-line_num+1;
    }
}

static void directory_screen_select(screen_t* screen) {
    directory_screen_t* self = screen->self;

    if (self->invalid) return;

	size_t path_length = strlen(self->path);
	self->path[path_length] = '/';
	strcpy(self->path+path_length+1, self->entries[self->selector]);

    FILINFO entry;
    f_stat(self->path, &entry);

    if (entry.fattrib & AM_DIR) {
        screen_push(directory_screen_new(self->path), ANIMATION_RIGHT);
    } else {
        audio_play(self->path);
        screen_push(playing_screen_get(), ANIMATION_TOP);
        strcpy(playing_screen_get()->background, screen->background);
    }

    self->path[path_length] = 0;
}

static void directory_screen_back(screen_t* screen) {
    screen_pop();
}

static void directory_screen_playing(screen_t* screen) {
    screen_push(playing_screen_get(), ANIMATION_TOP);
}

static void directory_screen_previous(screen_t* screen) {
    directory_screen_wheel(screen, -1);
}

static void directory_screen_next(screen_t* screen) {
    directory_screen_wheel(screen, +1);
}

static void directory_screen_delete(screen_t* screen) {
    directory_screen_t* self = screen->self;

    if (self->entry_buffer) free(self->entry_buffer);
    if (self->entries) free(self->entries);
    free(self);
    free(screen);
}

screen_t* directory_screen_new(char* path) {
    screen_t* screen = malloc(sizeof(screen_t));
    memset(screen, 0, sizeof(screen_t));

    directory_screen_t* self = malloc(sizeof(directory_screen_t));
    memset(self, 0, sizeof(directory_screen_t));
    screen->self = self;

    screen->draw = directory_screen_draw;
    screen->wheel = directory_screen_wheel;
    screen->select = directory_screen_select;
    screen->back = directory_screen_back;
    screen->playing = directory_screen_playing;
    screen->previous = directory_screen_previous;
    screen->next = directory_screen_next;
    screen->delete = directory_screen_delete;

	strcpy(self->path, path);
    directory_screen_open(screen);

    return screen;
}
