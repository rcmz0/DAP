#pragma once

#include "canvas.h"

typedef struct screen_t screen_t;

struct screen_t {
    void* self;
    char background[256];
    void (*draw)(screen_t* screen);
    void (*wheel)(screen_t* screen, int offset);
    void (*select)(screen_t* screen);
    void (*back)(screen_t* screen);
    void (*playing)(screen_t* screen);
    void (*previous)(screen_t* screen);
    void (*next)(screen_t* screen);
    void (*delete)(screen_t* screen);
};

typedef enum animation_t {
    ANIMATION_NONE,
    ANIMATION_LEFT,
    ANIMATION_RIGHT,
    ANIMATION_TOP,
    ANIMATION_BOTTOM,
} animation_t;

screen_t* screen_get_top();

void screen_wheel(int offset);
void screen_select();
void screen_back();
void screen_playing();
void screen_previous();
void screen_next();

void screen_replace(screen_t* screen, animation_t animation);
void screen_push(screen_t* screen, animation_t animation);
void screen_pop();

void screen_toggle();

void screen_draw(RGB565* framebuffer);
