#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "ff.h"

#include "ft2build.h"
#include FT_FREETYPE_H
#include "utf8.h"

#include "font.h"

typedef struct font_t {
	FIL file;
	FT_Face face;
	FT_StreamRec stream;
	FT_Open_Args open_args;
	int offset;
} font_t;

typedef struct font_config_t {
	char* path;
	int size;
	int offset;
} font_config_t;

static const font_config_t font_configs[] = {
	{
		.path = "0:/nanum.ttf",
		.size = 12,
		.offset = 0,
	},
	{
		.path = "0:/awesome.ttf",
		.size = 12,
		.offset = 2,
	},
};

#define num_fonts (sizeof(font_configs) / sizeof(*font_configs))
static font_t fonts[num_fonts];

static FT_Library ft_library;

#define font_cache_size 256
static glyph_t font_cache[font_cache_size];

static unsigned long ft_stream_read(FT_Stream stream, unsigned long offset, unsigned char* buffer, unsigned long count) {
	FIL* file = stream->descriptor.pointer;
	f_lseek(file, offset);
	if (buffer == NULL || count == 0) return 0;
	unsigned int bytes_read;
	f_read(file, buffer, count, &bytes_read);
	return bytes_read;
}

static void ft_stream_close(FT_Stream stream) {
	FIL* file = stream->descriptor.pointer;
	f_close(file);
}

void font_init() {
	FT_Init_FreeType(&ft_library);

	for (int i = 0; i < num_fonts; i++) {
		font_t* font = &fonts[i];
		font_config_t* font_config = (font_config_t*) &font_configs[i];

		f_open(&font->file, font_config->path, FA_READ);

		font->stream.read = ft_stream_read;
		font->stream.close = ft_stream_close;
		font->stream.descriptor.pointer = &font->file;
		font->stream.size = f_size(&font->file);

		font->open_args.flags = FT_OPEN_STREAM;
		font->open_args.stream = &font->stream;

		font->offset = font_config->offset;

		FT_Open_Face(ft_library, &font->open_args, 0, &font->face);

		FT_Set_Pixel_Sizes(font->face, 0, font_config->size);
	}

	memset(font_cache, 0, sizeof(font_cache));

	// preload ascii glyphs
	for (int codepoint = 0; codepoint < 128; codepoint++) {
		font_get_glyph(codepoint);
	}
}

static void font_load_glyph(int codepoint, glyph_t* glyph) {
	font_t* font = NULL;
	FT_Face face = NULL;
	uint index = 0;

	for (int i = 0; i < num_fonts; i++) {
		font = &fonts[i];
		face = font->face;
		index = FT_Get_Char_Index(face, codepoint);
		if (index) break;
	}

	FT_Load_Glyph(face, index, FT_LOAD_DEFAULT);
	FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

	glyph->codepoint = codepoint;
	glyph->offset_x = face->glyph->bitmap_left;
	glyph->offset_y = face->size->metrics.ascender/64 - face->glyph->bitmap_top + font->offset;
	glyph->width = face->glyph->bitmap.width;
	glyph->height = face->glyph->bitmap.rows;
	glyph->advance = face->glyph->metrics.horiAdvance/64;
	glyph->bitmap = malloc(glyph->width * glyph->height);
	memcpy(glyph->bitmap, face->glyph->bitmap.buffer, glyph->width * glyph->height);
}

glyph_t* font_get_glyph(int codepoint) {
	int i;

	for (i = 0; i < font_cache_size; i++) {
		if (font_cache[i].codepoint == codepoint) {
			return &font_cache[i];
		}
	}

	for (i = 0; i < font_cache_size; i++) {
		if (!font_cache[i].codepoint) {
			break;
		}
	}

	if (i == font_cache_size) {
		i = rand() % font_cache_size;
		// printf("[info cache] removed %x at %i\n", cache[i].codepoint, i);
		free(font_cache[i].bitmap);
	}

	glyph_t* glyph = &font_cache[i];
	font_load_glyph(codepoint, glyph);
	return glyph;
}

int font_get_text_width(char* text) {
	int text_width = 0;

	while (true) {
		int codepoint;
		text = utf8codepoint(text, &codepoint);
		if (codepoint == 0) break;

		// if (codepoint >= '0' && codepoint <= '9') {
		// 	text_width += 8;
		//  continue;
		// }

		glyph_t* glyph = font_get_glyph(codepoint);
		text_width += glyph->advance;
	}

	return text_width;
}

size_t font_get_cache_occupancy() {
	size_t occupancy = 0;

	for (int i = 0; i < font_cache_size; i++) {
		if (font_cache[i].codepoint != 0) {
			occupancy++;
		}
	}

	return occupancy;
}
