#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "ff.h"

#include "esp_log.h"
#include "esp_timer.h"
#include "driver/i2s.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define MINIMP3_ONLY_MP3
#define MINIMP3_IMPLEMENTATION
#include "minimp3.h"

#include "util.h"
#include "pins.h"
#include "audio.h"
#include "screen.h"
#include "playing_screen.h"

static TaskHandle_t audio_task;

static FIL file;
static int file_position;
static int file_duration;

static uint8_t file_buffer[16*1024];
static int file_buffer_length;

static bool pause_demanded = true;
static bool pause_achived = false;

static const int volume_max = 100;
static int volume = volume_max / 2;

static const int64_t volume_fade_time = 200000;
static int64_t volume_fade_until = 0;
static float volume_fade = 0;

static audio_info_t info = {};
static char queued_path[256] = {}; // should be protected by a mutex ?

static const int i2s_num = I2S_NUM_0;
static int i2s_channels;
static int i2s_depth;
static int i2s_samplerate;

static mp3dec_t mp3_decoder;
static int frame_num;
static float avg_bitrate;

static void audio_task_function(void* arg) {
	while (true) {
		float fade = clampf((float) (volume_fade_until - esp_timer_get_time()) / volume_fade_time, 0, 1);
		volume_fade = pause_demanded ? minf(volume_fade, fade) : maxf(volume_fade, 1 - fade);
		pause_achived = volume_fade == 0;

		// printf("pause demanded %i pause achived %i f_error %i\n", pause_demanded, pause_achived, f_error(&file));

		if (pause_achived) {
			if (*queued_path) {
				// printf("playing %s\n", queued_path);

				f_close(&file);
				f_open(&file, queued_path, FA_READ);

				file_duration = f_size(&file);
				file_position = 0;
				file_buffer_length = 0;
				frame_num = 0;
				mp3dec_init(&mp3_decoder);

				audio_resume();
				*queued_path = 0;
			} else {
				vTaskDelay(10);
			}
			continue;
		}
		
		size_t bytes_read;
		f_read(&file, file_buffer + file_buffer_length, sizeof(file_buffer) - file_buffer_length, &bytes_read);
		file_buffer_length += bytes_read;

		mp3dec_frame_info_t frame_info;
		static int16_t samples[MINIMP3_MAX_SAMPLES_PER_FRAME];
		int samples_num = mp3dec_decode_frame(&mp3_decoder, file_buffer, file_buffer_length, samples, &frame_info);

		file_buffer_length -= frame_info.frame_bytes;
		memmove(file_buffer, file_buffer + frame_info.frame_bytes, file_buffer_length);
		
		// end reached
		if (!frame_info.frame_bytes) {
			volume_fade = 0;
			audio_next();
			continue;
		}

		if (samples_num) {
			file_position += frame_info.frame_bytes;

			frame_num++;
			avg_bitrate = (avg_bitrate * (frame_num-1) + frame_info.bitrate_kbps) / frame_num;

			strcpy(info.codec, "MP3");
			info.bitrate = roundf(avg_bitrate);
			info.channels = frame_info.channels;
			info.depth = 16;
			info.samplerate = frame_info.hz;
			info.position = (file_position*8) / (avg_bitrate*1000);
			info.duration = (file_duration*8) / (avg_bitrate*1000);

			if (
				info.channels != i2s_channels || 
				info.depth != i2s_depth || 
				info.samplerate != i2s_samplerate
			) {
				i2s_channels = info.channels;
				i2s_depth = info.depth;
				i2s_samplerate = info.samplerate;
				i2s_set_clk(i2s_num, i2s_samplerate, i2s_depth, i2s_channels);
			}

			float volume_scale = powf((float) volume/volume_max * volume_fade, M_E);
			for (int i = 0; i < samples_num*info.channels; i++) samples[i] *= volume_scale;

			size_t i2s_bytes_written;
			i2s_write(i2s_num, samples, samples_num*info.channels*sizeof(int16_t), &i2s_bytes_written, portMAX_DELAY);
		} else {
			file_duration -= frame_info.frame_bytes;
		}
	}
}

void audio_init() {
	i2s_config_t i2s_config = {
		.mode = I2S_MODE_MASTER | I2S_MODE_TX,
		.sample_rate = 44100,
		.bits_per_sample = 16,
		.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
		.communication_format = I2S_COMM_FORMAT_STAND_I2S,
		.tx_desc_auto_clear = true,
		.dma_desc_num = 3,
		.dma_frame_num = 300,
		.intr_alloc_flags = ESP_INTR_FLAG_LEVEL2 | ESP_INTR_FLAG_IRAM,
	};
	ESP_ERROR_CHECK(i2s_driver_install(i2s_num, &i2s_config, 0, NULL));

	i2s_pin_config_t i2s_pin_config = {
		.ws_io_num = I2S_WS_PIN,
		.bck_io_num = I2S_BCK_PIN,
		.data_out_num = I2S_OUT_PIN,
		.mck_io_num = -1,
		.data_in_num = -1,
	};
	ESP_ERROR_CHECK(i2s_set_pin(i2s_num, &i2s_pin_config));

	xTaskCreate(audio_task_function, "audio", 32*1024, NULL, 24, &audio_task);
}

void audio_play(char* path) {
	strcpy(info.path, path);
	strcpy(queued_path, path);
	audio_pause();
}

void audio_previous() {
	if (!*info.path) return;

	if (info.position < 5) {
		char path[256];
		strcpy(path, info.path);

		char* current = strrchr(path, '/');
		*current = 0;
		current++;

		char previous[256];
		*previous = 0;

		FF_DIR dir;
		f_opendir(&dir, path);
		FILINFO entry;

		while(true) {
			f_readdir(&dir, &entry);
			if (!*entry.fname) break;

			if (!strcmp(entry.fname, current)) {
				if (*previous) {
					strcat(path, "/");
					strcat(path, previous);

					audio_play(path);

					if (screen_get_top() == playing_screen_get()) {
						screen_replace(screen_get_top(), ANIMATION_LEFT);
					}

					f_closedir(&dir);
					return;
				}
				break;
			}

			char* extension = strrchr(entry.fname, '.');
			if (!strcmp(extension, ".mp3")) {
				strcpy(previous, entry.fname);
			}
		}

		f_closedir(&dir);
	}

	audio_play(info.path);
}

void audio_next() {
	if (!*info.path) return;

	char path[256];
	strcpy(path, info.path);

	char* current = strrchr(path, '/');
	*current = 0;
	current++;

	FF_DIR dir;
	f_opendir(&dir, path);
	FILINFO entry;

    while (true) {
        f_readdir(&dir, &entry);
        if (!*entry.fname) break;

		if (!strcmp(entry.fname, current)) {
			while (true) {
				f_readdir(&dir, &entry);
				if (!*entry.fname) break;

				char* extension = strrchr(entry.fname, '.');
				if (!strcmp(extension, ".mp3")) {
					strcat(path, "/");
					strcat(path, entry.fname);

					audio_play(path);

					if (screen_get_top() == playing_screen_get()) {
						screen_replace(screen_get_top(), ANIMATION_RIGHT);
					}
					break;
				}
			}
			break;
		}
	}

	f_closedir(&dir);
}

bool audio_is_paused() {
	return pause_demanded && !*queued_path;
}

void audio_pause() {
	if (!*info.path) return;
	pause_demanded = true;
	volume_fade_until = esp_timer_get_time() + volume_fade_time;
}

void audio_resume() {
	if (!*info.path) return;
	pause_demanded = false;
	volume_fade_until = esp_timer_get_time() + volume_fade_time;
}

void audio_toggle() {
	if (audio_is_paused()) {
		audio_resume();
	} else {
		audio_pause();
	}
}

int audio_get_volume() {
	return volume;
}

void audio_set_volume(int value) {
	volume = clamp(value, 0, volume_max);
}

void audio_offset_volume(int offset) {
	audio_set_volume(audio_get_volume() + offset);
}

audio_info_t* audio_get_info() {
	return &info;
}
