#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "math.h"

#include "esp_timer.h"

#include "util.h"
#include "performance_analyzer.h"

#define window_w 64
#define window_h 8
#define print_period 1

static float frametimes[window_w];

static void performance_analyzer_print() {
    float min_frametime = infinityf();
    float max_frametime = 0;
    float avg_frametime = 0;

    for (int i = 0; i < window_w; i++) {
        min_frametime = min(min_frametime, frametimes[i]);
        max_frametime = max(max_frametime, frametimes[i]);
        avg_frametime = (avg_frametime * i + frametimes[i]) / (i + 1);
    }

    printf("current:%f min:%f avg:%f max:%f\n", frametimes[window_w - 1], min_frametime, avg_frametime, max_frametime);

    // int graph_ys[window_w];
    // for (int x = 0; x < window_w; x++) {
    //     graph_ys[x] = roundf(map(frametimes[x], min_frametime, max_frametime, 0, window_h-1));
    // }

    // char graph[(window_w + 1) * window_h + 1];
    // for (int y = 0; y < window_h; y++) {
    //     for (int x = 0; x < window_w; x++) {
    //         int i = (window_w + 1) * y + x;
    //         graph[i] = (y == graph_ys[x]) ? '#' : ' ';
    //     }
    //     graph[(window_w + 1) * y + window_w] = '\n';
    // }
    // graph[(window_w + 1) * window_h] = 0;
    // printf(graph);
}

void performance_analyzer_tick() {
    static int64_t previous_tick = 0;
    int64_t tick = esp_timer_get_time();
    float frametime = (float) (tick - previous_tick) / 1000;
    previous_tick = tick;

    memmove(frametimes, frametimes + 1, (window_w - 1) * sizeof(float));
    frametimes[window_w - 1] = frametime;

    static int frame = 0;
    frame++;
    if (frame % print_period == 0) performance_analyzer_print();
}
